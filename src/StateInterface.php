<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use Stringable;

/**
 * StateInterface interface file.
 *
 * A State represtents a stable status for the subject object. This status is
 * the starting point or the ending point of a transition.
 *
 * @author Anastaszor
 */
interface StateInterface extends Stringable
{
	
	/**
	 * Gets the name of this state.
	 *
	 * @return string
	 */
	public function getName() : string;
	
}
