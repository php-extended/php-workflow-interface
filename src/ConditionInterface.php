<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use Stringable;

/**
 * ConditionInterface interface file.
 *
 * A condition represents a test that should be passed by the subject in order
 * to enable the transition's passage.
 *
 * @author Anastaszor
 */
interface ConditionInterface extends Stringable
{
	
	/**
	 * Gets whether the subject satisfies the given condition.
	 *
	 * @param SubjectInterface $subject
	 * @return boolean true if satisfied, false else
	 */
	public function isSatisfiedBy(SubjectInterface $subject) : bool;
	
}
