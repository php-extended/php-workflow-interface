<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use InvalidArgumentException;
use RuntimeException;
use Stringable;

/**
 * SubjectInterface interface file.
 *
 * A subject is an object which can change its state according to a specific
 * workflow. A subject may change various states based on different workflows.
 *
 * @author Anastaszor
 */
interface SubjectInterface extends Stringable
{
	
	/**
	 * Gets an unique id for this object.
	 * 
	 * @return string
	 */
	public function getId() : string;
	
	/**
	 * Gets the state of the subject, according to this workflow.
	 *
	 * @param WorkflowInterface $workflow
	 * @return StateInterface
	 */
	public function getState(WorkflowInterface $workflow) : StateInterface;
	
	/**
	 * Gets whether this subject can perform the given transition for the given
	 * workflow.
	 * 
	 * @param WorkflowInterface $workflow
	 * @param TransitionInterface $transition
	 * @return boolean
	 */
	public function can(WorkflowInterface $workflow, TransitionInterface $transition) : bool;
	
	/**
	 * Executes the given transition for the given workflow.
	 *
	 * @param WorkflowInterface $workflow
	 * @param TransitionInterface $transition
	 * @return StateInterface the state of the subject at the end of the transition
	 * @throws InvalidArgumentException if the transition cannot be performed
	 * @throws RuntimeException if the execution failed and cannot be rolled back
	 */
	public function perform(WorkflowInterface $workflow, TransitionInterface $transition) : StateInterface;
	
}
