<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use Stringable;

/**
 * TransitionInterface interface file.
 *
 * A Transition represents a link between a starting state and a ending state.
 *
 * @author Anastaszor
 */
interface TransitionInterface extends Stringable
{
	
	/**
	 * Gets the name of this transition.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the state in which the subject should be before the transition
	 * is performed.
	 *
	 * @return StateInterface
	 */
	public function getStartState() : StateInterface;
	
	/**
	 * Gets the state in which the subject will be once the transition is
	 * performed.
	 *
	 * @return StateInterface
	 */
	public function getEndState() : StateInterface;
	
	/**
	 * Checks whether the subject satisfies the transition's conditions.
	 *
	 * @param SubjectInterface $subject
	 * @return boolean true if the transition's conditions are satisfied
	 */
	public function isSatisfiedBy(SubjectInterface $subject) : bool;
	
}
