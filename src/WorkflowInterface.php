<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use Stringable;

/**
 * WorkflowInterface interface file.
 *
 * This interface defines a workflow. A workflow is a process for which an
 * business object (which can represent a group of other objects) follows a
 * state changing path, leading to changing its variables and its behavior
 * from state to state.
 *
 * Such workflow declares on this object statuses (called State) which
 * represents a stable state of the object that follows the workflow. The
 * workflow also declares transition conditions (called Transition) which
 * defines why and how the subject object may change state from one state to
 * the next. All states and transitions must be declared into the workflow
 * definition.
 *
 * @author Anastaszor
 */
interface WorkflowInterface extends Stringable
{
	
	/**
	 * Gets the name of this workflow.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the current state of the subject.
	 * 
	 * @param SubjectInterface $subject
	 * @return StateInterface the current state of the subject
	 */
	public function getState(SubjectInterface $subject) : StateInterface;
	
	/**
	 * Gets whether this workflow allows the subject to perform the transition
	 * named with $transitionName.
	 *
	 * @param SubjectInterface $subject
	 * @param TransitionInterface $transition
	 * @return boolean true if the transition is possible, else false
	 */
	public function can(SubjectInterface $subject, TransitionInterface $transition) : bool;
	
	/**
	 * Effectively performs the transition on the subject with the given
	 * $transitionName.
	 *
	 * @param SubjectInterface $subject
	 * @param TransitionInterface $transition
	 * @return StateInterface the state after the transition is performed, 
	 *                        successfully, or failed
	 */
	public function perform(SubjectInterface $subject, TransitionInterface $transition) : StateInterface;
	
}
