<?php declare(strict_types=1);

/*
 * This file is part of the php-extended/php-workflow-interface library
 *
 * (c) Anastaszor
 * This source file is subject to the MIT license that
 * is bundled with this source code in the file LICENSE.
 */

namespace PhpExtended\Workflow;

use Iterator;
use Stringable;

/**
 * DefinitionInterface interface file.
 *
 * A Definition is an object that describes all the possible stable states of
 * the subject object, and all the transitions that may be possible for the
 * subject to use to jump from one stable state to the next.
 *
 * @author Anastaszor
 */
interface DefinitionInterface extends Stringable
{
	
	/**
	 * Gets the name of the workflow.
	 *
	 * @return string
	 */
	public function getName() : string;
	
	/**
	 * Gets the possible states in the definition.
	 *
	 * @return Iterator<StateInterface>
	 */
	public function getStates() : Iterator;
	
	/**
	 * Gets the possible transitions in the definition.
	 *
	 * @return Iterator<TransitionInterface>
	 */
	public function getTransitions() : Iterator;
	
}
